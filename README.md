# go-qs

moved to [[https://bitbucket.org/mindaugas_z/go-silent-snake/src/master/libs/]]

Queue Subscribe

```
    q := qs.New()

	q.Subscribe("test", func(i qs.Item) {
		println(i.(string))
	})

	q.Publish("test", "1st")
	q.Publish("test", "2nd")
```