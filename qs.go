package qs

import (
	"bitbucket.org/mindaugas_z/go-deq"
	"sync"
)

type Item interface{}

func New() *T {
	t := new(T)
	t.mu = &sync.RWMutex{}
	t.q = make(map[string]*deq.T)
	t.us = make(map[string]func())
	return t
}

type T struct {
	mu *sync.RWMutex
	q  map[string]*deq.T
	us map[string]func()
}

func (t *T) Subscribe(name string, fn func(Item)) {
	t.mu.Lock()
	q, ok := t.q[name]
	if !ok {
		t.q[name] = deq.New()
		q = t.q[name]
	}
	t.us[name] = q.Subscribe(func(i deq.Item) {
		fn(i)
	})
	t.mu.Unlock()
}

func (t *T) Unsubscribe(name string) {
	if _, ok := t.us[name]; ok {
		t.mu.Lock()
		t.us[name]()
		delete(t.us, name)
		t.mu.Unlock()
	}
}

func (t *T) Publish(name string, item Item) {
	t.mu.Lock()
	q, ok := t.q[name]
	if ok {
		q.WritePrimary(item)
	}
	t.mu.Unlock()
}
